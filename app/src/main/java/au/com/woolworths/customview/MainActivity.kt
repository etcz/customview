package au.com.woolworths.customview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private lateinit var fuelGauge: Fuelable
    private val subs = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Timber.plant(Timber.DebugTree())

        fuelGauge = findViewById<View>(R.id.fuel_gauge) as Fuelable
    }

    override fun onResume() {
        super.onResume()
        subs.add(buildFuelNumberStream().subscribe {
            fuelGauge.fuel = it
            Timber.d("Fuel set to $it")
        })

    }

    override fun onPause() {
        super.onPause()
        subs.clear()
    }

    private fun buildFuelNumberStream(): Observable<Int> {
        return Observable.interval(50, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map { interval ->
                    80 - interval.rem(80L).toInt()
                }
    }
}
