package au.com.woolworths.customview

import android.content.Context
import android.graphics.PointF
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.layout_fuel_gauge.view.*

class MyCustomViewK @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), Fuelable {

    /**
     * MAX_GAUGE_DEGREES is the angle at which the needle is exactly pointed to the full tank
     * indicator/marker and on the empty marker (when flipped horizontally).
     *
     * MAX_FUEL_VALUE is an arbitrary number to denote a value that corresponds to a full tank.
     */
    companion object {
        const val MAX_FUEL_VALUE = 80
        const val MAX_GAUGE_DEGREES = 70
    }

    /**
     * Based on a rough estimate the circular "head" of the needle (in needle.png) is roughly 4
     * times lesser than its height.
     */
    private val needleHeadDiameter by lazy { needleView.height / 4f }

    override var fuel: Int = 0
        set(value) {
            post(updateRunnable)
            field = value
        }

    private val updateRunnable = { updateFuelGauge(fuel.toFloat()) }

    init {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.layout_fuel_gauge, this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeCallbacks(updateRunnable)
    }

    /**
     * On every update, we calculate the mapping from the following range of numbers (in descending
     * order):
     *
     * [MAX_FUEL_VALUE, 0] (e.g. [80, 0])
     *
     * to the the following range of numbers:
     *
     * [MAX_GAUGE_DEGREES, -MAX_GAUGE_DEGREES] (e.g.  [70, -70])
     *
     * Mapping descending ranges results to the needle going counter-clockwise (from full tank to
     * empty), which is what we want here. Essentially we need to set the rotation from
     * +70deg to -70deg (or +290deg).
     */
    private fun updateFuelGauge(value: Float) {
        val midpoint = MAX_FUEL_VALUE / 2f
        val angle = if (value > midpoint) {
            (value - midpoint) / midpoint * MAX_GAUGE_DEGREES
        } else {
            (midpoint - value) / midpoint * -MAX_GAUGE_DEGREES
        }
        needleView.pivotX = calculatePivot().x
        needleView.pivotY = calculatePivot().y
        needleView.rotation = angle
    }

    /**
     * The pivot point should be at exactly the middle of the needleView (ImageView)'s x-axis and
     * slightly above the bottom (i.e y-coordinate). By slightly above, we mean, it's offsetted by
     * the radius of the circle which can be found at the end of the needle.
     *
     * It is at this point where we need to rotate the needle by so the rotation stays in-place.
     */
    private fun calculatePivot(): PointF {
        val x = needleView.width / 2f
        val y = needleView.height.toFloat() - (needleHeadDiameter / 2f)
        return PointF(x, y)
    }
}

